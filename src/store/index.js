import { createStore } from 'vuex'

export default createStore({
  state: {
  	nav: {
  		enableTransparency: true,
  		isTransparent: true,
  	}
  },
  mutations: {
  	SET_NAV_TRANSPARENCY_ENABLED (state, isEnabled) {
  		state.nav.enableTransparency = isEnabled
  	},

  	SET_NAV_TRANSPARENT (state, isTransparent) {
  		state.nav.isTransparent = isTransparent
  	},
  },
  getters: {
  	isNavTransparencyEnabled: state => state.nav.enableTransparency,
  	isNavTransparent: 		  state => state.nav.isTransparent,
  },
  actions: {
  },
  modules: {
  }
})
